# SEGURANÇA DE APLICAÇÃO #

### ASP NET IDENTITY ###

- Evolução do member ship
- Integração com EF nHibernate
- Integração com Banco de Dados
- Testavel
- Integração com Rede Social (Activity Director)

### CONCEITOS ###

- Confidêncialidade : Garantir a segurança de dados;

- Integridade: Garantir a integração dos dados para não serem alterados ou adulterados. 

- Autenticação: O sistema deve garantir quem é exatamente o usuário, e dar a autenticação para acessar o sistema.

- Níveis de autenticação: Criar niveis (perfil) de acesso para cada action do sistema.