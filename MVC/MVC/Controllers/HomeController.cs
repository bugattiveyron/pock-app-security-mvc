﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVC.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        /// <summary>
        /// Adicionando Regras de Roles para permissão de usuario
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles ="Analista")] //Deve estar Logado e deve ser analista // [Authorize] -> apenas deve estar logado
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}